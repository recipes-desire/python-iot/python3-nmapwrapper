import os

from .external_configuration import get_default_section as get_default_config_data
""" Host Discovery 
"""


def get_host_discovery_cidr():
    #cidr = os.environ["LOCAL-NETWORK-CIDR"]
    #cidr = "192.168.5.0/24"
    cidr = get_default_config_data()["scan-network-cidr"]

    # Return Local-Network-CIDR from OS-Environment
    return cidr


def get_host_discovery_command():
    #command = os.environ["HOST-DISCOVERY-COMMAND"]
    command = "nmap -v -sn"

    # Return nmap with necessary parameters
    return command


""" Port Scanning 
"""


def get_port_scanning_command():
    #command = os.environ["PORT-SCANNING-COMMAND"]
    command = "nmap -sT --top-ports 1000 -v -oG - "

    #SCAN_TCP = "nmap -sT --top-ports 1000 -v -oG - "
    #SCAN_UDP = "sudo nmap -sU --top-ports 1000 -v -oG - "
    #SCAN_TCP_UDP = "sudo nmap -sU -sT "

    # Return the appropriate form of nmap for open port-scanning
    return command


""" NMAP Version  
"""


def get_nmap_version_command():
    command = "nmap --version"
    return command


NMAP_VERSION_COMMAND = get_nmap_version_command()

HOST_DISCOVERY_COMMAND = get_host_discovery_command()
HOST_DISCOVERY_CIDR = get_host_discovery_cidr()
HOST_DISCOVERY_DICT = {
    'command': HOST_DISCOVERY_COMMAND,
    'cidr': HOST_DISCOVERY_CIDR
}

PORT_SCANNING_COMMAND = get_port_scanning_command()

__all__ = [
    "NMAP_VERSION_COMMAND", "HOST_DISCOVERY_DICT", "PORT_SCANNING_COMMAND"
]
